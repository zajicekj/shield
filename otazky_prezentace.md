# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :--- | :--- |
| jak dlouho mi tvorba zabrala - **čistý čas** | 12h + 3h |
| jak se mi to podařilo rozplánovat | vše šlo dle plánu, až na debugging |
| design zapojení | ve složce schéma |
| proč jsem zvolil tento design | jednoduchý, přímočarý|
| zapojení |  |
| z jakých součástí se zapojení skládá | kabely |
| realizace | ve slozce fotky |
| nápad, v jakém produktu vše propojit dohromady|  |
| co se mi povedlo |  |
| co se mi nepovedlo/příště bych udělal/a jinak | nastavení pinů |
| zhodnocení celé tvorby | frustrující |