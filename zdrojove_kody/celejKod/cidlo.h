#include <OneWire.h>
#include <DallasTemperature.h>

#define ONE_WIRE_BUS 12

OneWire oneWire(ONE_WIRE_BUS);


DallasTemperature sensors(&oneWire);

void tempSetup() {
  Serial.begin(9600);
 
  sensors.begin();
}
