// knihovna
#include <Adafruit_NeoPixel.h>
// pin
int pasekPin = 0;
// pocet LED
int pocet = 8;
// inicializace
Adafruit_NeoPixel pasek = Adafruit_NeoPixel(pocet, pasekPin, NEO_GRB + NEO_KHZ800);

void barva (byte r, byte g, byte b, int cislo) {
  // promenna barvy
  int konkretniB;
  // načtení barvy do proměnné
  konkretniB = pasek.Color(r, g, b);
  // nastavení barvy - poradi od nuly
  pasek.setPixelColor(cislo - 1, konkretniB);
  // aktualizace
  pasek.show();
}
void zkouska() {
  
  // ruzova
  barva(255, 0, 255, 1);
  delay(1000);
  barva(255, 0, 255, 2);
  delay(1000);
  barva(255, 0, 255, 3);
  delay(1000);
  barva(255, 0, 255, 4);
  delay(1000);
  barva(255, 0, 255, 5);
  delay(1000);
  barva(255, 0, 255, 6);
  delay(1000);
  barva(255, 0, 255, 7);
  delay(1000);
  barva(255, 0, 255, 8);
}
void off(){
   
  barva(0  , 0  , 0  , 1);
  barva(0  , 0  , 0  , 2);
  barva(0  , 0  , 0  , 3);
  barva(0  , 0  , 0  , 4);
  barva(0  , 0  , 0  , 5);
  barva(0  , 0  , 0  , 6);
  barva(0  , 0  , 0  , 7);
  barva(0  , 0  , 0  , 8);

  }
 
void bomba() {
  byte cervena = random(0, 255);
  byte zelena = random(0, 255);
  byte modra = random(0, 255);
  byte dioda = random(1, (pocet + 1));
  barva(cervena, zelena, modra, dioda);
}


  
