#include "cidlo.h"
#include "displej.h"
#include "led.h"
#include "pasek.h"
#include "rezistor.h"


void setup() {
  tempSetup();
  Display();
  led();
  pasek.begin();
}

void loop() {
  blik();
  displayPhotoValue();
  delay(1000);
  displayCelsius();
  zkouska();
  delay(500);
  
  off();
  
  for (int i = 0; i < 200; i++) {
       bomba();
       delay(50);
   }
  off();

  }
  

void displayCelsius() {
   
  lcd.init();
  
  sensors.requestTemperatures();
  lcd.setCursor(0, 0);
  // Print a message to the LCD.
  lcd.print("Celsius temp: ");
  lcd.setCursor(0, 1);
  lcd.print(sensors.getTempCByIndex(0));
  Serial.print("Teplota: ");
  Serial.println(sensors.getTempCByIndex(0));
}


void displayPhotoValue(){
  lcd.init();
  lcd.setCursor(0, 0);
  lcd.print("Photoresistor: ");
  lcd.setCursor(0, 1);
  lcd.print(hodnotaFoto());
  Serial.print("Photo: ");
  Serial.println(hodnotaFoto());
}
