// knihovny
#include <OneWire.h>
#include <DallasTemperature.h>

// pin
int cidlo = 12;
// instance
OneWire oneWire(cidlo);
DallasTemperature senzor(&oneWire);

void setup(void) {
  // komunikace
  Serial.begin(9600);
  senzor.begin();
}

void loop(void) {
  // zjistí teplotu
  senzor.requestTemperatures();
  // výpis teploty
  Serial.print("Teplota: ");
  Serial.print(senzor.getTempCByIndex(0));
  Serial.println("° C");
  
  delay(1000);
  }
