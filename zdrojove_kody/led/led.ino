int red = 16;
int green = 5;

void setup() {
 
  pinMode(red, OUTPUT);
  pinMode(green, OUTPUT);
}


void loop() {
  digitalWrite(red, LOW); 
  digitalWrite(green, LOW);
  delay(1000);
  digitalWrite(red, HIGH);   
  digitalWrite(green, HIGH);    
  delay(1000);                       
}
