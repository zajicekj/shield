



#include <LiquidCrystal_I2C.h>

// radky a sloupce
int sloupce = 16;
int radky = 2;

// adresa, sloupce a radky
LiquidCrystal_I2C lcd(0x27, sloupce, radky);  

void setup(){
  // inicializace
  lcd.init();
  // zadni svetlo se zapne                     
  lcd.backlight();
}

void loop(){
  // nastavi kurzor na zacatek
  lcd.setCursor(0, 0);
  // vypise zpravu
  lcd.print("kni");
  delay(1000);
  // vycistit displej
  lcd.clear();
  // ´kurzor na druhou radku
  lcd.setCursor(0,1);
  lcd.print("hovna");
  delay(1000);
  lcd.clear(); 
}
